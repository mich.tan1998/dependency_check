import gdown
from zipfile import ZipFile
from glob import glob
import os 
# place your google drive link 
url = "https://drive.google.com/drive/folders/11TRPmjwVBSww30xAzXLvAQGj27mCt2ZF?usp=sharing"
output = "./env"
gdown.download_folder(url, output=output, quiet=True, use_cookies=False)

zip_files = glob('./env/*.zip')

if not zip_files:
    raise ValueError("No zip files found in the specified directory.")

for z in zip_files:
    with ZipFile(z) as zf:
        zf.extractall("./env")

    os.remove(z)

