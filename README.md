# OWASP Dependency-Check

Dependency-Check is a Software Composition Analysis (SCA) tool that attempts to detect publicly disclosed vulnerabilities contained within a project’s dependencies. It does this by determining if there is a Common Platform Enumeration (CPE) identifier for a given dependency. If found, it will generate a report linking to the associated CVE entries.

## Things to Take Notes:

1. **Create a Gmail Account** for Google Drive Usage:
    - It is recommended that the project team create a shared Gmail account specifically for google drive usage. This account will be used to manage and store environment files required for scanning.
2. Use the **Correct Google Drive links** for Dependency Check:
    - Always verify that you are using the correct link for dependency checks. This ensures that the correct environment is being scanned
3. Manage **GitLab Free Tier Pipeline** Minutes:
    - Keep in mind that with the GitLab Free Tier, you have a limit of 400 pipeline minutes per month. It is advisable to use these minutes wisely, especially during ad-hoc testing
